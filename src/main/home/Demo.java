import model.Employee;
import org.hibernate.cfg.Configuration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import service.EmployeeService;

/**
 * Created by Sania on 15.08.2017.
 */
public class Demo {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
        EmployeeService service = (EmployeeService) context.getBean("employeeService");

        System.out.println(service.getAllEmployee());


    }
}
