package repository.impl;

import model.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import repository.EmployeeRepository;

import java.util.List;

/**
 * Created by Sania on 05.08.2017.
 */
@Repository(value = "employeeRepository")
public class EmployeeRepositoryImpl implements EmployeeRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Employee> getAllEmployee() {
        Session session;
        List<Employee> employees;
        session = sessionFactory.openSession();
        employees = (List<Employee>) session.createQuery("from Employee").list();
        return employees;
    }
}
