package service;

import model.Employee;
import repository.EmployeeRepository;

import java.util.List;

/**
 * Created by Sania on 05.08.2017.
 */
public interface EmployeeService {
    List<Employee> getAllEmployee();

}
